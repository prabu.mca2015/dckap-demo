<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="{{ asset('js/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

      
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    <div class="container mt-5">

     <a class="btn btn-success float-right" href="{{ url('/sku/create') }}" role="button">Add SKU</a><br><br>

            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-success">
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">SKU</th>
                        <th scope="col">Short Description</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                        <th scope="col">Images</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employeeData as $key => $data)
                    <tr>
                        <th scope="row">{{ $key+1 }}</th>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->sku }}</td>
                        <td>{{ $data->price }}</td>
                        <td>{{ $data->short_description }}</td>
                        <td>{{ $data->description }}</td>
                        <td>
                        @if(isset($data->getFiles))
                            @foreach($data->getFiles as $keys => $datas)
                                <b><a target='_blank' href="{{$datas->append_url}}" class="card-link">{{$datas->name}}</a></b> <br>
                            @endforeach
                        @endif
                        </td>
                        <td>{{ $data->is_status }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{url('sku/'.encrypt($data->id).'/edit')}}">Edit</a>
                                    <form action="{{ url('sku/'.encrypt($data->id)) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    <button  class="dropdown-item">Delete</button>
                                    {{ csrf_field() }}
                                    </form>
                                    <!-- <a class="dropdown-item" href="">Delete</a> -->
                                  
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                {!! $employeeData->links() !!}
            </div>

        </div>
    </body>
</html>
