<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="{{ asset('js/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        
        <link rel="stylesheet" href="{{asset('js/jquery-ui/jquery-ui.min.css')}}"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

      
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    <div class="container mt-5">

    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        @foreach ($errors->all() as $error)
        {{ $error }}
        @endforeach
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    <h4>{{$errors->first()}}</h4>
    @endif

    <h5> <span style="    font-weight: bold;border-bottom: 5px solid #28a745"> Add SKU</span>  <a class="btn btn-success float-right" href="{{ url('/') }}" role="button">Back</a></h5> <br>
   
     <form  id="add_sku_form" method="POST" action="{{ url('sku/') }}"  enctype="multipart/form-data" accept-charset="utf-8">
        @csrf
        <div class="form-row">
            <div class="col-md-7 mb-4">
                <label for="validationServer01">Name</label>
                <input type="text" class="form-control" placeholder="Enter the Name"  data-validation="required length custom" name = "name" data-validation-length="3-100" maxlength="100" data-validation-regexp="^([a-zA-Z0-9&. ]+)$" data-validation-error-msg-custom="The input value must be alphanumerics and specified special characters(&,. and space)"  value="{{ old('name') }}">
              
            </div>
            <div class="col-md-5 mb-4">
                <label for="validationServer02">SKU</label>
                <input type="text" class="form-control" name="sku" data-validation="required length " data-validation-length="2-20" maxlength="20" data-validation-regexp="^([a-zA-Z0-9]+)$" data-validation-error-msg-custom="The input value must be alphanumerics" placeholder="SKU"  value="{{ old('sku') }}">
             
            </div>
           
        </div>

        <div class="form-row">
            <div class="col-md-12 mb-4">
                <label for="exampleFormControlTextarea1"> Short Description</label>
                <textarea class="form-control" name="short_description"  data-validation="required length custom" data-validation-regexp="^((?!(&)).)*$" data-validation-length="3-500" maxlength="500" data-validation-error-msg-custom="The input value should not allow ampersand symbol(&)" rows="2">{{ old('description') }}</textarea>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12 mb-4">
                <label for="exampleFormControlTextarea1"> Description</label>
                <textarea class="form-control" name="description" data-validation="required length custom" data-validation-regexp="^((?!(&)).)*$" data-validation-length="3-5000" maxlength="5000" data-validation-error-msg-custom="The input value should not allow ampersand symbol(&)" rows="4">{{ old('description') }}</textarea>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-12 mb-4">
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file"  name="files[]" class="custom-file-input" required multiple>
                        <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                    </div>                   
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-6 mb-4">
                <label for="validationServer01">Prices</label>
                <input type="number" class="form-control" name="price"  data-validation="required length custom" placeholder="Prices"  data-validation-length="1-20" maxlength="20"   value="{{ old('price') }}">
              
            </div>
            <div class="col-md-6 mb-4">               
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Status</label>
                    <select class="form-control" name="status" data-validation="required" id="exampleFormControlSelect1">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>                  
                    </select>
                </div>
            </div>
           
        </div>

       
        <button class="btn btn-primary" type="submit">Submit</button>
        </form>
        </div>

        <script type="text/javascript" src="{{asset('js/jquery-form-validator/jquery.form-validator.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui/jquery-ui.min.js')}}" ></script>

        <script>
        	$.validate({
                form: '#add_sku_form',
                modules : '',
                inputParentClassOnError: 'has-danger',
                validateOnBlur: false,
                onError : function($form) {
                return false;
                },
                onSuccess : function($form) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    
                     return rtue; 
                //event.preventDefault();
                
                }
            });
        </script>
    </body>
</html>
