<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDckap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dckap', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('sku', 20);
            $table->double('price', 8, 2);
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->smallInteger('status')->default(1);
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dckap');
    }
}
