<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DCKAPModel;
use App\Models\Files;

use Illuminate\Support\Str;

use Validator;
use Redirect;
use Storage;

class CRUDController extends Controller
{

    protected $dckapPModel;

    public function __construct(DCKAPModel $dckapPModel) 
    {
        $this->dckapPModel = $dckapPModel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeData =  $this->dckapPModel->with('getFiles')->paginate(5);
        return view('index', compact('employeeData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $input = $request->except('_token');

        $validator = Validator::make($input, [
            'name' => 'required',
            'sku' => 'required|unique:dckap,sku',
            'price' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'price' => 'required',
            'status' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:png,txt,jpeg,jpg,pdf',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        } else {
            if($request->hasfile('files'))
            {
               
                $insert = [];
                // dd($data);
                $id = $this->dckapPModel->create($input)->id;
                foreach($request->file('files') as $key => $file)
                {
                    $fileName = Str::random(8).'.'.$file->extension();  
                    $file->move(public_path('files'), $fileName);
                    $name = $file->getClientOriginalName();
        
                    $insert['name'] = $name;
                    $insert['path'] = $fileName;
                    $insert['dckap_id'] = $id;
                    Files::create($insert);
                }
                
               
                return Redirect::to('/');
            }
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeeData =  $this->dckapPModel->with('getFiles')->findOrFail(decrypt($id));
        return view('edit', compact('employeeData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');
        $encryptId = decrypt($id);
        $validator = Validator::make($input, [
            'name' => 'required',
            'sku' =>'required|unique:dckap,sku,'.$encryptId.',id',
            'price' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'price' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        } else {
            $updateData = $this->dckapPModel->find($encryptId);
            if($request->hasfile('files'))
            {
                $files = Files::where('dckap_id',$encryptId)->get();
                foreach ($files as $key => $value) {
                    Storage::disk('public')->delete($value->path);
                }
                Files::where('dckap_id',$encryptId)->delete();
                
                $insert = [];
                foreach($request->file('files') as $key => $file)
                {
                    $fileName = Str::random(8).'.'.$file->extension();  
                    $file->move(public_path('files'), $fileName);
                    $name = $file->getClientOriginalName();
        
                    $insert['name'] = $name;
                    $insert['path'] = $fileName;
                    $insert['dckap_id'] = $encryptId;
                    Files::create($insert);
                }
            }
            $updateData->update($input);
               
                return Redirect::to('/');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $encryptId = decrypt($id);
        $this->dckapPModel->find($encryptId)->delete();
        return Redirect::to('/');
    }
}
