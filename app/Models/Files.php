<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Files extends Model
{
    use HasFactory;
    public $fillable = ['dckap_id','name','path'];

    protected $appends = ['append_url'];

    public function getAppendURLAttribute() {
        return Storage::disk('public')->url($this->path);
    }
}
