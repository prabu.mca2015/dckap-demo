<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DCKAPModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'dckap';

    public $fillable = ['name','sku','price','short_description','description','status'];

    protected $appends = ['is_status'];


     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function getIsStatusAttribute()
    {
        return ($this->status == 1) ? 'Active' : 'Inactive';
    }


    public function getFiles() {
        return $this->hasMany('App\\Models\\Files', 'dckap_id');
    }
}
